from typing import List, Dict, Any, Tuple
from enum import Enum

NO_TRAIN = "--no-train"
NO_PLOT = "--no-plot"
NO_VERBOSE = "--no-verbose"

##################################
# Hardcoded parameters
DATA_FILENAME = "./data.bin"
SHOTS = "361"
RECEIVERS = "361"
SAMPLES = "1251"
WINDOW_SIZE = "250x250"
CHECKPOINT_DIR = "./training_checkpoints"
TRAIN_RATIO = "0.7"
BATCH_SIZE = "1"
GEN_LEARNING_RATE = "1e-4"
DISC_LEARNING_RATE = "1e-4"
LOSS_NORM = "l1"
LAMBDA = "1000"
KERNEL = "3"
STEPS = "100000"
RUN_ID = "0"
##################################


class Parameter:
    name: str
    value: Any = None
    default_value: str
    parse_func: Any
    inv_parse_func: Any

    def __init__(self, name: str, default_value: str, parse: Any, inverse_parse: Any):
        self.name = name
        self.default_value = default_value
        self.parse_func = parse
        self.inv_parse_func = inverse_parse

    def parse(self, value: str) -> Any:
        self.value = self.parse_func(value)
        return self.value

    def inv_parse(self) -> str:
        return self.inv_parse_func(self.value)


class IntParameter(Parameter):
    def __init__(self, name: str, default_value: str):
        super().__init__(name, default_value, lambda x: int(x), lambda x: str(x))


class FloatParameter(Parameter):
    def __init__(self, name: str, default_value: str):
        super().__init__(name, default_value, lambda x: float(x), lambda x: str(x))


class StrParameter(Parameter):
    def __init__(self, name: str, default_value: str):
        super().__init__(name, default_value, lambda x: x, lambda x: x)


class ParameterName(Enum):
    DATA_FILENAME = "data_filename"
    SHOTS = "shots"
    RECEIVERS = "receivers"
    SAMPLES = "samples"
    WINDOW_SIZE = "window_size"
    CHECKPOINT_DIR = "checkpoint_dir"
    TRAIN_RATIO = "train_ratio"
    BATCH_SIZE = "batch_size"
    GEN_LEARNING_RATE = "gen_learning_rate"
    DISC_LEARNING_RATE = "disc_learning_rate"
    LOSS_NORM = "loss_norm"
    LAMBDA = "lambda"
    KERNEL = "kernel"
    STEPS = "steps"
    RUN_ID = "run_id"


class NormType(Enum):
    L1 = "l1"
    L2 = "l2"


parameters_list: Dict[ParameterName, Parameter] = {
    ParameterName.DATA_FILENAME: StrParameter(ParameterName.DATA_FILENAME.value, DATA_FILENAME),
    ParameterName.SHOTS: IntParameter(ParameterName.SHOTS.value, SHOTS),
    ParameterName.RECEIVERS: IntParameter(ParameterName.RECEIVERS.value, RECEIVERS),
    ParameterName.SAMPLES: IntParameter(ParameterName.SAMPLES.value, SAMPLES),
    ParameterName.WINDOW_SIZE: Parameter(
        ParameterName.WINDOW_SIZE.value,
        WINDOW_SIZE,
        lambda x: (int(x.split("x")[0]), int(x.split("x")[1])),
        lambda x: f"{x[0]}x{x[1]}",
    ),
    ParameterName.CHECKPOINT_DIR: StrParameter(ParameterName.CHECKPOINT_DIR.value, CHECKPOINT_DIR),
    ParameterName.TRAIN_RATIO: FloatParameter(ParameterName.TRAIN_RATIO.value, TRAIN_RATIO),
    ParameterName.BATCH_SIZE: IntParameter(ParameterName.BATCH_SIZE.value, BATCH_SIZE),
    ParameterName.GEN_LEARNING_RATE: FloatParameter(ParameterName.GEN_LEARNING_RATE.value, GEN_LEARNING_RATE),
    ParameterName.DISC_LEARNING_RATE: FloatParameter(ParameterName.DISC_LEARNING_RATE.value, DISC_LEARNING_RATE),
    ParameterName.LOSS_NORM: Parameter(ParameterName.LOSS_NORM.value, LOSS_NORM, lambda x: NormType(x), lambda x: x.value),
    ParameterName.LAMBDA: FloatParameter(ParameterName.LAMBDA.value, LAMBDA),
    ParameterName.KERNEL: IntParameter(ParameterName.KERNEL.value, KERNEL),
    ParameterName.STEPS: IntParameter(ParameterName.STEPS.value, STEPS),
    ParameterName.RUN_ID: IntParameter(ParameterName.RUN_ID.value, RUN_ID),
}


def get_args_parameters_values(args: List[str]) -> Dict[str, str]:
    parameters = {}
    for arg in args:
        if "=" in arg:
            param_value = arg.split("=")
            parameters[param_value[0]] = param_value[1]
    return parameters


def get_checkpoint_full_dir(parameters: Dict[ParameterName, Parameter]) -> str:
    dir = parameters[ParameterName.CHECKPOINT_DIR].inv_parse() + "/"
    first = True
    for param in parameters.values():
        if param.name != ParameterName.CHECKPOINT_DIR.value:
            dir += f"{'' if first else '_'}{param.name}_{param.inv_parse().split('/')[-1] if param.name == ParameterName.DATA_FILENAME.value else param.inv_parse()}"
            first = False
    return dir


def get_input_parameters(args: List[str]) -> Tuple[Dict[ParameterName, Parameter], bool, bool, bool]:
    if NO_TRAIN in args:
        no_train = True
    else:
        no_train = False
    if NO_PLOT in args:
        no_plot = True
    else:
        no_plot = False
    if NO_VERBOSE in args:
        verbose = False
    else:
        verbose = True

    args_parameters = get_args_parameters_values(args)

    for param in parameters_list.values():
        if param.name in args_parameters:
            param.parse(args_parameters[param.name])
        else:
            param.parse(param.default_value)

    msg = [
        "",
        "###############################################",
        "Using parameters:",
        "",
    ]
    msg += [f"{param.name}={param.inv_parse()}" for param in parameters_list.values()]
    msg += ["###############################################"]
    print("\n".join(msg))

    return parameters_list, get_checkpoint_full_dir(parameters_list), verbose, no_train, no_plot
