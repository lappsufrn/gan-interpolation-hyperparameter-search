import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import sys

from data_management import prepare_data
from training_configs import GAN, generate_images, plot_images_from_windows, test_model
from utils import get_input_parameters, ParameterName


############################################
# Get input parameters
(
    parameters,
    checkpoint_full_dir,
    verbose,
    no_train,
    no_plot,
) = get_input_parameters(sys.argv)


data_filename = parameters[ParameterName.DATA_FILENAME].value
receivers = parameters[ParameterName.RECEIVERS].value
shots = parameters[ParameterName.SHOTS].value
samples = parameters[ParameterName.SAMPLES].value
window_size = parameters[ParameterName.WINDOW_SIZE].value
train_ratio = parameters[ParameterName.TRAIN_RATIO].value
batch_size = parameters[ParameterName.BATCH_SIZE].value
gen_learning_rate = parameters[ParameterName.GEN_LEARNING_RATE].value
disc_learning_rate = parameters[ParameterName.DISC_LEARNING_RATE].value
loss_norm = parameters[ParameterName.LOSS_NORM].value
LAMBDA = parameters[ParameterName.LAMBDA].value
kernel = parameters[ParameterName.KERNEL].value
steps = parameters[ParameterName.STEPS].value
run_id = parameters[ParameterName.RUN_ID].value


############################################
# Preparing data for training
(
    shot_gather,
    shot_decimated,
    shot_gather_256,
    shot_decimated_256,
    old_boundaries,
    windows_starting_points,
    num_windows,
) = prepare_data(data_filename, shots, receivers, samples, window_size, verbose=verbose)


############################################
# Configuring network
test_model(shot_gather_256[150], kernel)
gan = GAN(
    shot_decimated_256,
    shot_gather_256,
    train_ratio,
    batch_size,
    gen_learning_rate,
    disc_learning_rate,
    loss_norm,
    LAMBDA,
    kernel,
    steps,
    checkpoint_dir=checkpoint_full_dir,
    no_train=no_train,
)
gan.restore_checkpoint()


############################################
# Training
if not no_train:
    gan.fit(steps=steps) # Add "calculate_test_norm_loss=False" if you do not want to calculate the loss norm of the entire test set every 100 steps
    gan.restore_checkpoint()


############################################
# Plotting results
if not no_plot:

    window_id = -1
    generate_images(gan.generator, shot_decimated_256[window_id].reshape((1,256,256,1)), shot_gather_256[window_id].reshape((1,256,256,1)), plot_dir=checkpoint_full_dir+"/figures/")

    shot_id = 360
    plot_images_from_windows(
        shot_decimated_256, shot_gather_256, shot_id, gan.generator, num_windows, window_size, windows_starting_points, plot_dir=checkpoint_full_dir+"/figures/"
    )
