# Hyperparameter determination for GAN-based seismic interpolator with variable neighborhood search

Codes and implementations for the problem of [Hyperparameter determination for GAN-based seismic interpolator with variable neighborhood search](https://doi.org/10.1016/j.cageo.2024.105689).

## Installing packages
To install the required packages, run:
```
python -m venv .venv
source .venv/bin/activate
python -m pip install -r requirements.txt
```
Note: the packages require Python>=3.10.

Before using this program, remember to always activate the Python environment:
```
source .venv/bin/activate
```

## Training and ploting seismograms for a specific set of hyperparameters

The GAN interpolator can be trained with [GAN_Interpolation.py](GAN_Interpolation.py) script. There are two options to select the parameters and hyperparameters. The first one is by hardcoding them in [utils.py](utils.py) and running:
```
python GAN_Interpolation.py [--no-train] [--no-plot] [--no-verbose]
```
The options `--no-train`, `--no-plot` and `--no-verbose` will prevent training, plotting and verbosing, respectively.

The second option to select the parameters and hyperparameters is using command line arguments by running:
```
python GAN_Interpolation.py [data_filename=<name>] [shots=<shots>] [receivers=<receivers>] [...] [--no-train] [--no-plot] [--no-verbose]
```
This is the list of parameters and their default values, which are given in [utils.py](utils.py):
  - data_filename=./data.bin
  - shots=361
  - receivers=361
  - samples=1251
  - window_size=250x250
  - checkpoint_dir=./training_checkpoints
  - train_ratio=0.7
  - batch_size=1
  - gen_learning_rate=1e-4
  - disc_learning_rate=1e-4
  - loss_norm=l1
  - lambda=1000
  - kernel=3
  - steps=100000
  - run_id=0

For example, by running:
```
python GAN_Interpolation.py --no-train checkpoint_dir=vns_ckpts/ train_ratio=0.7 gen_learning_rate=0.00390625 disc_learning_rate=0.00390625 lambda=128.0
```
it will plot the results (without training due to the `--no-train` option) given by a GAN whose weights are stored in the directory `vns_ckpts/`, using a train-test ratio of 70%, learning rates of 0.00390625 and $\lambda$ = 128. Note that the figures will be saved in pdf format inside the directory of the respective configuration.

## Running VNS:

The search for hyperparameters can be done by simply running:
```
import vns
v = vns.VNS()
v.run()
```
This is what [Hyperparameters_search.py](Hyperparameters_search.py) does. You can use the function `vns.VNS.plot_performance()` to plot the performance graph of the search. We implemented the search so that it can be paused or canceled at any time, and the search will continue where it stoped when it start again.

## Plotting graphs of exhaustive searches

The script [plot_results.py](plot_results.py) will plot the graphs of the influence of each hyperparameter by summing up the results of a "grid-search" methodology, in which we train GAN for every combination of preselected sets of hyperparameters.

Edit the selected values to be considered, for example:
```
parameters = [
    ("checkpoint_dir", ["./training_checkpoints"]),
    ("loss_norm", ["l1", "l2"]),
    ("train_ratio", ["0.7"]),
    ("gen_learning_rate", ["1e-4", "2e-4", "4e-4"]),
    ("disc_learning_rate", ["1e-4", "2e-4", "4e-4"]),
    ("batch_size", ["1", "2"]),
    ("lambda", ["10", "100", "1000"]),
    ("kernel", ["3", "4", "5", "6"]),
    ("steps", ["100000"]),
    ("run_id", ["0"]),
]
```
