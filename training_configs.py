from typing import Tuple
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import os
import datetime
import time
from IPython import display

from data_management import get_shots_from_windows, resizeneg
from utils import NormType

BUFFER_SIZE = 500
TEST_BUFFER_SIZE = 50


class GAN:
    start_time = time.time()
    train_dataset: tf.data.Dataset
    test_dataset: tf.data.Dataset
    train_steps: int
    generator: tf.keras.Model
    discriminator: tf.keras.Model
    LAMBDA: float
    loss_norm: NormType
    loss_object = tf.keras.losses.BinaryCrossentropy(from_logits=True)
    generator_optimizer: tf.keras.optimizers.Adam
    discriminator_optimizer: tf.keras.optimizers.Adam
    checkpoint_dir: str
    checkpoint_prefix: str
    checkpoint: tf.train.Checkpoint
    log_dir: str
    summary_writer: tf.summary.SummaryWriter

    def __init__(
        self,
        input: np.ndarray,
        target: np.ndarray,
        train_ratio: float,
        batch_size: int,
        gen_learning_rate: float,
        disc_learning_rate: float,
        loss_norm: NormType,
        LAMBDA: float,
        kernel: int,
        train_steps: int,
        train_buffer_size: int = BUFFER_SIZE,
        test_buffer_size: int = TEST_BUFFER_SIZE,
        checkpoint_dir: str = "./training_checkpoints",
        log_dir: str = "logs/",
        no_train: bool = False,
    ):
        self.train_dataset, self.test_dataset = split_train_test(
            input, target, train_ratio, batch_size, train_buffer_size, test_buffer_size
        )
        self.train_steps = train_steps
        self.LAMBDA = LAMBDA
        self.generator = Generator(kernel)
        self.discriminator = Discriminator(kernel)
        self.generator_optimizer = tf.keras.optimizers.Adam(gen_learning_rate, beta_1=0.5)
        self.discriminator_optimizer = tf.keras.optimizers.Adam(disc_learning_rate, beta_1=0.5)
        self.loss_norm = loss_norm
        self.checkpoint_dir = checkpoint_dir
        self.checkpoint_prefix = os.path.join(checkpoint_dir, "ckpt")
        self.checkpoint = tf.train.Checkpoint(
            generator_optimizer=self.generator_optimizer,
            discriminator_optimizer=self.discriminator_optimizer,
            generator=self.generator,
            discriminator=self.discriminator,
        )
        self.log_dir = log_dir
        self.summary_writer = tf.summary.create_file_writer(log_dir + "fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))
        if not no_train:
            os.makedirs(
                self.checkpoint_dir, exist_ok=True
            )  # Set "exist_ok=False" if you want the job to terminate if results already exist

    def generator_loss(self, disc_generated_output, gen_output, target):
        gan_loss = self.loss_object(tf.ones_like(disc_generated_output), disc_generated_output)
        # Mean absolute error
        if self.loss_norm == NormType.L1:
            norm_loss = tf.reduce_mean(tf.abs(target - gen_output))
        elif self.loss_norm == NormType.L2:
            norm_loss = tf.reduce_mean(tf.square(target - gen_output))
        else:
            raise Exception()
        total_gen_loss = gan_loss + (self.LAMBDA * norm_loss)
        return total_gen_loss, gan_loss, norm_loss

    def discriminator_loss(self, disc_real_output, disc_generated_output):
        real_loss = self.loss_object(tf.ones_like(disc_real_output), disc_real_output)
        generated_loss = self.loss_object(tf.zeros_like(disc_generated_output), disc_generated_output)
        total_disc_loss = real_loss + generated_loss
        return total_disc_loss

    @tf.function
    def train_step(self, input_image, target, step):
        with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
            gen_output = self.generator(input_image, training=True)
            disc_real_output = self.discriminator([input_image, target], training=True)
            disc_generated_output = self.discriminator([input_image, gen_output], training=True)

            gen_total_loss, gen_gan_loss, gen_norm_loss = self.generator_loss(disc_generated_output, gen_output, target)
            disc_loss = self.discriminator_loss(disc_real_output, disc_generated_output)
        generator_gradients = gen_tape.gradient(gen_total_loss, self.generator.trainable_variables)
        discriminator_gradients = disc_tape.gradient(disc_loss, self.discriminator.trainable_variables)

        self.generator_optimizer.apply_gradients(zip(generator_gradients, self.generator.trainable_variables))
        self.discriminator_optimizer.apply_gradients(zip(discriminator_gradients, self.discriminator.trainable_variables))

        with self.summary_writer.as_default():
            tf.summary.scalar("gen_total_loss", gen_total_loss, step=step // 1000)
            tf.summary.scalar("gen_gan_loss", gen_gan_loss, step=step // 1000)
            tf.summary.scalar("gen_norm_loss", gen_norm_loss, step=step // 1000)
            tf.summary.scalar("disc_loss", disc_loss, step=step // 1000)

        return gen_total_loss, gen_gan_loss, gen_norm_loss, disc_loss
    
    @tf.function
    def get_norm_loss_over_test_set(self):
        cost = 0.0
        for (input_image, target) in self.test_dataset:
            gen_output = self.generator(input_image, training=True)
            cost += tf.reduce_sum(tf.abs(target - gen_output))
        return cost

    def fit(self, steps, calculate_test_norm_loss=True):
        start = time.time()

        for step, (input_image, target) in self.train_dataset.repeat().take(steps).enumerate():
            if (step) % 1000 == 0:
                display.clear_output(wait=True)

                if step != 0:
                    print(f"Time taken for 1000 steps: {time.time()-start:.2f} sec\n")

                start = time.time()

                print(f"Step: {step//1000}k")

            gen_total_loss, gen_gan_loss, gen_norm_loss, disc_loss = self.train_step(input_image, target, step)

            if step == 0 or (step + 1) % 100 == 0:
                with open(self.checkpoint_dir + "/graphs.txt", "a") as f:
                    if calculate_test_norm_loss:
                        test_norm_loss = self.get_norm_loss_over_test_set()
                        f.write(
                            f"{step}   {time.time()-self.start_time}   {gen_total_loss.numpy()}   {gen_gan_loss.numpy()}   {gen_norm_loss.numpy()}   {disc_loss.numpy()}   {test_norm_loss}\n"
                        )
                    else:
                        f.write(
                            f"{step}   {time.time()-self.start_time}   {gen_total_loss.numpy()}   {gen_gan_loss.numpy()}   {gen_norm_loss.numpy()}   {disc_loss.numpy()}\n"
                        )

            # Training step
            if (step + 1) % 10 == 0:
                print(".", end="", flush=True)

            # Save (checkpoint) the model every "self.train_steps" steps
            if (step + 1) % self.train_steps == 0:
                self.checkpoint.save(file_prefix=self.checkpoint_prefix)

    def restore_checkpoint(self):
        self.checkpoint.restore(tf.train.latest_checkpoint(self.checkpoint_dir))


def split_train_test(
    data: np.ndarray, target: np.ndarray, train_ratio: float, batch_size: int, train_buffer_size: int = 500, test_buffer_size: int = 50
):
    assert train_ratio > 0 and train_ratio <= 1
    splitter = int(train_ratio * data.shape[0])

    train_dataset = tf.data.Dataset.from_tensor_slices((data[:splitter], target[:splitter]))
    test_dataset = tf.data.Dataset.from_tensor_slices((data[splitter:], target[splitter:]))

    train_dataset = train_dataset.shuffle(train_buffer_size)
    train_dataset = train_dataset.batch(batch_size)

    test_dataset = test_dataset.shuffle(test_buffer_size)
    test_dataset = test_dataset.batch(batch_size)

    return train_dataset, test_dataset


def downsample(filters, size, apply_batchnorm=True):
    initializer = tf.random_normal_initializer(0.0, 0.02)
    result = tf.keras.Sequential()
    result.add(tf.keras.layers.Conv2D(filters, size, strides=2, padding="same", kernel_initializer=initializer, use_bias=False))

    if apply_batchnorm:
        result.add(tf.keras.layers.BatchNormalization())
    result.add(tf.keras.layers.LeakyReLU())

    return result


def upsample(filters, size, apply_dropout=False):
    initializer = tf.random_normal_initializer(0.0, 0.02)
    result = tf.keras.Sequential()
    result.add(
        tf.keras.layers.Conv2DTranspose(filters, size, strides=2, padding="same", kernel_initializer=initializer, use_bias=False)
    )

    result.add(tf.keras.layers.BatchNormalization())
    if apply_dropout:
        result.add(tf.keras.layers.Dropout(0.5))
    result.add(tf.keras.layers.ReLU())

    return result


def Generator(kernel):
    inputs = tf.keras.layers.Input(shape=[256, 256, 1])

    down_stack = [
        downsample(64, kernel, apply_batchnorm=False),  # (batch_size, 128, 128, 64)
        downsample(128, kernel),  # (batch_size, 64, 64, 128)
        downsample(256, kernel),  # (batch_size, 32, 32, 256)
        downsample(512, kernel),  # (batch_size, 16, 16, 512)
        downsample(512, kernel),  # (batch_size, 8, 8, 512)
        downsample(512, kernel),  # (batch_size, 4, 4, 512)
        downsample(512, kernel),  # (batch_size, 2, 2, 512)
        downsample(512, kernel),  # (batch_size, 1, 1, 512)
    ]

    up_stack = [
        upsample(512, kernel, apply_dropout=True),  # (batch_size, 2, 2, 1024)
        upsample(512, kernel, apply_dropout=True),  # (batch_size, 4, 4, 1024)
        upsample(512, kernel, apply_dropout=True),  # (batch_size, 8, 8, 1024)
        upsample(512, kernel),  # (batch_size, 16, 16, 1024)
        upsample(256, kernel),  # (batch_size, 32, 32, 512)
        upsample(128, kernel),  # (batch_size, 64, 64, 256)
        upsample(64, kernel),  # (batch_size, 128, 128, 128)
    ]

    initializer = tf.random_normal_initializer(0.0, 0.02)
    last = tf.keras.layers.Conv2DTranspose(
        1,
        kernel,
        strides=2,
        padding="same",
        kernel_initializer=initializer,
        activation="tanh",
    )  # (batch_size, 256, 256, 3)

    x = inputs

    # Downsampling through the model
    skips = []
    for down in down_stack:
        x = down(x)
        skips.append(x)
    skips = reversed(skips[:-1])

    # Upsampling and establishing the skip connections
    for up, skip in zip(up_stack, skips):
        x = up(x)
        x = tf.keras.layers.Concatenate()([x, skip])
    x = last(x)

    return tf.keras.Model(inputs=inputs, outputs=x)


def Discriminator(kernel):
    initializer = tf.random_normal_initializer(0.0, 0.02)

    inp = tf.keras.layers.Input(shape=[256, 256, 1], name="input_image")
    tar = tf.keras.layers.Input(shape=[256, 256, 1], name="target_image")

    x = tf.keras.layers.concatenate([inp, tar])  # (batch_size, 256, 256, channels*2)

    down1 = downsample(64, kernel, False)(x)  # (batch_size, 128, 128, 64)
    down2 = downsample(128, kernel)(down1)  # (batch_size, 64, 64, 128)
    down3 = downsample(256, kernel)(down2)  # (batch_size, 32, 32, 256)

    zero_pad1 = tf.keras.layers.ZeroPadding2D()(down3)  # (batch_size, 34, 34, 256)
    conv = tf.keras.layers.Conv2D(512, kernel, strides=1, kernel_initializer=initializer, use_bias=False)(
        zero_pad1
    )  # (batch_size, 31, 31, 512)
    batchnorm1 = tf.keras.layers.BatchNormalization()(conv)
    leaky_relu = tf.keras.layers.LeakyReLU()(batchnorm1)
    zero_pad2 = tf.keras.layers.ZeroPadding2D()(leaky_relu)  # (batch_size, 33, 33, 512)
    last = tf.keras.layers.Conv2D(1, kernel, strides=1, kernel_initializer=initializer)(zero_pad2)  # (batch_size, 30, 30, 1)

    return tf.keras.Model(inputs=[inp, tar], outputs=last)


def test_model(shot, kernel):
    # Testing downsample
    down_model = downsample(64, kernel)
    print(tf.expand_dims(shot, 0).shape)
    down_result = down_model(tf.expand_dims(shot, 0))
    print(down_result.shape)

    # Testing upsample
    up_model = upsample(1, kernel)
    up_result = up_model(down_result)
    print(up_result.shape)


def generate_images(model, test_input, tar, plot_dir="figures/"):
    prediction = model(test_input, training=True)

    display_list = [
        test_input[0, :, :],
        tar[0, :, :],
        prediction[0, :, :],
        tar[0, :, :] - prediction[0, :, :],
    ]
    title = ["Input Image", "True Image", "Predicted Image", "Differences"]

    os.makedirs(plot_dir, exist_ok=True)
    for i in range(4):
        plt.figure(figsize=(5, 5))
        plt.axes([0, 0, 1, 1])
        plt.imshow(np.transpose(display_list[i], [1, 0, 2]), cmap="gray", vmin=-0.05, vmax=0.05, aspect="auto")
        plt.savefig(f"{plot_dir}/seismograms_{title[i].split()[0].lower()}.pdf", format="pdf")
        plt.close()
    # plt.show()


def generate_outputs(model: tf.keras.Model, inputs: np.ndarray):
    predictions = np.zeros((inputs.shape[0], inputs.shape[1], inputs.shape[2], 1))
    for i, input in enumerate(inputs):
        predictions[i] = model(input[tf.newaxis, ...], training=True)
    return predictions


def plot_images_from_windows(
    input: np.ndarray,
    target: np.ndarray,
    shot_id: int,
    model: tf.keras.Model,
    num_windows: Tuple[int, int],
    window_size: Tuple[int, int],
    windows_starting_points: np.ndarray,
    plot_dir: str = "figures/",
):
    samples_per_shot = num_windows[0] * num_windows[1]

    input = input[shot_id * samples_per_shot : (shot_id + 1) * samples_per_shot]
    output = generate_outputs(model, input)
    target = target[shot_id * samples_per_shot : (shot_id + 1) * samples_per_shot]

    input = resizeneg(input, window_size[0], window_size[1])
    output = resizeneg(output, window_size[0], window_size[1])
    target = resizeneg(target, window_size[0], window_size[1])

    input_shot = get_shots_from_windows(input, window_size, windows_starting_points, num_windows)
    output_shot = get_shots_from_windows(output, window_size, windows_starting_points, num_windows)
    target_shot = get_shots_from_windows(target, window_size, windows_starting_points, num_windows)

    display_list = [input_shot[0], target_shot[0], output_shot[0], target_shot[0] - output_shot[0]]

    title = ["Input Image", "True Image", "Predicted Image", "Differences"]

    plt.figure(figsize=(15, 15))
    for i in range(4):
        plt.axes([0.0025+0.25*i, 0.01, 0.245, 0.97])
        plt.title(title[i])
        plt.imshow(np.transpose(display_list[i], [1, 0, 2]), cmap="gray", vmin=-0.05, vmax=0.05, aspect="auto")
        plt.axis("off")
    plt.savefig(f"{plot_dir}/seismograms_full.pdf", format="pdf")
    # plt.show()
    plt.close()
