from typing import List, Tuple
from enum import Enum
from random import shuffle
import numpy as np
import pandas as pd
import io
import time
import os
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

from data_management import prepare_data
from training_configs import GAN, generate_images
from utils import get_input_parameters, NormType


class X:
    Lambda: int
    GLR: int
    DLR: int
    kernel: int

    known_evaluations = pd.DataFrame(data={"Lambda": [], "GLR": [], "DLR": [], "kernel": [], "cost": []})

    def __init__(self, Lambda: int = 7, GLR: int = -8, DLR: int = -8, kernel: int = 3):
        self.Lambda = Lambda
        self.GLR = GLR
        self.DLR = DLR
        self.kernel = kernel

    def evaluate(self) -> float:
        print(f"Evaluating Lambda = 2^{self.Lambda}, GLR = 2^{self.GLR}, DLR = 2^{self.DLR}, kernel = {self.kernel}...")
        print(X.known_evaluations)
        find_self = X.known_evaluations[
            (X.known_evaluations["Lambda"] == self.Lambda)
            & (X.known_evaluations["GLR"] == self.GLR)
            & (X.known_evaluations["DLR"] == self.DLR)
            & (X.known_evaluations["kernel"] == self.kernel)
        ]
        if len(find_self) > 0:
            print("Found in X.known_evaluations!")
            cost = find_self["cost"].iloc[0]
        else:
            data_filename = "./data.bin"
            shots = 361
            receivers = 361
            samples = 1251
            window_size = (250, 250)
            checkpoint_dir = "./vns_ckpts"
            loss_norm = NormType("l1")
            train_ratio = 0.7
            batch_size = 1
            steps = 100000
            gen_learning_rate = 2 ** self.GLR
            disc_learning_rate = 2 ** self.DLR
            Lambda = 2 ** self.Lambda
            kernel = self.kernel
            run_id = 0

            (_, checkpoint_full_dir, _, _, _) = get_input_parameters(
                [
                    f"data_filename={data_filename}",
                    f"shots={shots}",
                    f"receivers={receivers}",
                    f"samples={samples}",
                    f"window_size={window_size[0]}x{window_size[1]}",
                    f"checkpoint_dir={checkpoint_dir}",
                    f"loss_norm={loss_norm.value}",
                    f"train_ratio={train_ratio}",
                    f"batch_size={batch_size}",
                    f"steps={steps}",
                    f"gen_learning_rate={gen_learning_rate}",
                    f"disc_learning_rate={disc_learning_rate}",
                    f"lambda={Lambda}",
                    f"kernel={kernel}",
                    f"run_id={run_id}",
                ]
            )

            (
                shot_gather,
                shot_decimated,
                shot_gather_256,
                shot_decimated_256,
                old_boundaries,
                windows_starting_points,
                num_windows,
            ) = prepare_data(data_filename, shots, receivers, samples, window_size, verbose=False)

            try:
                g = np.loadtxt(checkpoint_full_dir + "/graphs.txt")
                if g[-1, 0] == steps - 1:
                    finished = True
                else:
                    finished = False
            except:
                finished = False
            if finished:
                no_train = True
                no_plot = True
            else:
                no_train = False
                no_plot = False
                if os.path.isdir(checkpoint_full_dir):
                    os.system(f"rm -r {checkpoint_full_dir}")

            gan = GAN(
                shot_decimated_256,
                shot_gather_256,
                train_ratio,
                batch_size,
                gen_learning_rate,
                disc_learning_rate,
                loss_norm,
                Lambda,
                kernel,
                steps,
                checkpoint_dir=checkpoint_full_dir,
                no_train=no_train,
            )
            gan.restore_checkpoint()

            if not no_train:
                gan.fit(steps=steps, calculate_test_norm_loss=False)
                gan.restore_checkpoint()

            if not no_plot:
                window_id = -1
                generate_images(
                    gan.generator,
                    shot_decimated_256[window_id].reshape((1, 256, 256, 1)),
                    shot_gather_256[window_id].reshape((1, 256, 256, 1)),
                    plot_dir=checkpoint_full_dir + "/figures/",
                )

            # Get cost
            test_input = shot_decimated_256[int(train_ratio * shot_decimated_256.shape[0]) :]
            test_target = shot_gather_256[int(train_ratio * shot_decimated_256.shape[0]) :]
            test_predicted = np.array([gan.generator(input.reshape((1, 256, 256, 1)), training=True)[0] for input in test_input])

            # cost = np.sqrt(((test_target - test_predicted) ** 2).sum())
            cost = abs(test_target - test_predicted).sum()
            X.known_evaluations.loc[len(X.known_evaluations)] = {
                "Lambda": self.Lambda,
                "GLR": self.GLR,
                "DLR": self.DLR,
                "kernel": self.kernel,
                "cost": cost,
            }
        print(f"Cost: {cost}")
        return cost


class Neighbor(Enum):
    next_lambda = "next lambda"
    previous_lambda = "previous lambda"
    next_GLR = "next GLR"
    previous_GLR = "previous GLR"
    next_DLR = "next DLR"
    previous_DLR = "previous DLR"
    next_kernel = "next kernel"
    previous_kernel = "previous kernel"


class Move:
    x: X
    direction: Neighbor

    def __init__(self, x: X, direction: Neighbor):
        self.x = x
        self.direction = direction


class VNS:
    x: X
    x_cost: float
    shaking_x: X = None
    shaking_x_cost: float = None
    start_from_local_search: bool = True
    local_search_starts_with_x: bool = True
    local_search_starts_with_shaking_x: bool = False
    local_search_starts_with_local_search_x: bool = False
    local_search_x: X = None
    local_search_x_cost: float = None
    k: int
    k_max: int
    start_time: float
    elapsed_time: float
    log_file: str
    log_writer: io.TextIOWrapper

    def __init__(
        self,
        x: X = X(),
        k_max: int = 4,
        k: int = 1,
        elapsed_time: float = 0.0,
        log_file: str = "vns_log.txt",
        look_for_checkpoint: bool = True,
    ):
        self.log_file = log_file
        if look_for_checkpoint:
            ckpt = self.get_checkpoint()
        else:
            ckpt = None
        if ckpt is None:
            self.start_time = time.time()
            self.x = x
            self.k = k
            self.x_cost = self.x.evaluate()
            self.elapsed_time = elapsed_time
            self.open_log_file(append=False)
            self.write_to_log(
                f"{'[Init]':>15} cost: {self.x_cost:<15.3} time: {self.get_elapsed_time():<15.3f} k: {self.k:2} parameters: Lambda=2^{self.x.Lambda}   GLR=2^{self.x.GLR}   DLR=2^{self.x.DLR}   kernel={self.x.kernel}\n"
            )
        else:
            self.k = ckpt["k"]
            self.x = ckpt["x"]
            self.shaking_x = ckpt["shaking_x"]
            self.local_search_x = ckpt["local_search_x"]
            self.x_cost = self.x.evaluate()
            if not self.shaking_x is None:
                self.shaking_x_cost = self.shaking_x.evaluate()
            if not self.local_search_x is None:
                self.local_search_x_cost = self.local_search_x.evaluate()
            self.elapsed_time = ckpt["elapsed_time"]
            self.open_log_file()
            self.start_time = time.time() - self.elapsed_time
        self.k_max = k_max

    def get_checkpoint(self):
        try:
            with open(self.log_file) as f:
                lines = f.readlines()
            if lines[-1].split()[0] == "[Init]":
                self.start_from_local_search = True
                self.local_search_starts_with_x = True
                self.local_search_starts_with_shaking_x = False
                self.local_search_starts_with_local_search_x = False
            elif lines[-1].split()[0] == "[Shaking]":
                self.start_from_local_search = True
                self.local_search_starts_with_x = False
                self.local_search_starts_with_shaking_x = True
                self.local_search_starts_with_local_search_x = False
            elif lines[-1].split()[0] == "[LocalSearch]":
                self.start_from_local_search = True
                self.local_search_starts_with_x = False
                self.local_search_starts_with_shaking_x = False
                self.local_search_starts_with_local_search_x = True
            elif lines[-1].split()[0] == "[Update]":
                self.start_from_local_search = False
                self.local_search_starts_with_x = False
                self.local_search_starts_with_shaking_x = True
                self.local_search_starts_with_local_search_x = False
            else:
                return None

            init_lines = [line for line in lines if line.split()[0] == "[Init]"]
            if init_lines:
                _, _, _, x = VNS.get_data_from_log_line(init_lines[-1])
            else:
                x = None

            shaking_lines = [line for line in lines if line.split()[0] == "[Shaking]"]
            if shaking_lines:
                _, _, _, shaking_x = VNS.get_data_from_log_line(shaking_lines[-1])
            else:
                shaking_x = None

            local_search_lines = [line for line in lines if line.split()[0] == "[LocalSearch]"]
            if local_search_lines:
                _, _, _, local_search_x = VNS.get_data_from_log_line(local_search_lines[-1])
            else:
                local_search_x = None

            update_lines = [line for line in lines if line.split()[0] == "[Update]"]
            if update_lines:
                _, _, _, x = VNS.get_data_from_log_line(update_lines[-1])

            _, elapsed_time, k, _ = VNS.get_data_from_log_line(lines[-1])
            return {
                "k": k,
                "elapsed_time": elapsed_time,
                "x": x,
                "shaking_x": shaking_x,
                "local_search_x": local_search_x,
            }
        except FileNotFoundError:
            return None

    def get_data_from_log_line(line: str) -> Tuple[float, float, int, X]:
        sp = line.split()
        cost = float(sp[2])
        elapsed_time = float(sp[4])
        k = int(sp[6])
        Lambda = int(sp[8].split("=2^")[-1])
        GLR = int(sp[9].split("=2^")[-1])
        DLR = int(sp[10].split("=2^")[-1])
        kernel = int(sp[11].split("=")[-1])
        return cost, elapsed_time, k, X(Lambda=Lambda, GLR=GLR, DLR=DLR, kernel=kernel)

    def open_log_file(self, append: bool = True):
        if append:
            self.log_writer = open(self.log_file, "a")
        else:
            self.log_writer = open(self.log_file, "w")

    def write_to_log(self, s: str):
        self.log_writer.write(s)
        self.log_writer.flush()

    def get_elapsed_time(self) -> float:
        self.elapsed_time = time.time() - self.start_time
        return self.elapsed_time

    def find_neighbors(x: X) -> List[Move]:
        if x.kernel > 2:
            neighbors = [
                Move(X(Lambda=x.Lambda + 1, GLR=x.GLR, DLR=x.DLR, kernel=x.kernel), Neighbor.next_lambda),
                Move(X(Lambda=x.Lambda - 1, GLR=x.GLR, DLR=x.DLR, kernel=x.kernel), Neighbor.previous_lambda),
                Move(X(Lambda=x.Lambda, GLR=x.GLR + 1, DLR=x.DLR, kernel=x.kernel), Neighbor.next_GLR),
                Move(X(Lambda=x.Lambda, GLR=x.GLR - 1, DLR=x.DLR, kernel=x.kernel), Neighbor.previous_GLR),
                Move(X(Lambda=x.Lambda, GLR=x.GLR, DLR=x.DLR + 1, kernel=x.kernel), Neighbor.next_DLR),
                Move(X(Lambda=x.Lambda, GLR=x.GLR, DLR=x.DLR - 1, kernel=x.kernel), Neighbor.previous_DLR),
                Move(X(Lambda=x.Lambda, GLR=x.GLR, DLR=x.DLR, kernel=x.kernel + 1), Neighbor.next_kernel),
                Move(X(Lambda=x.Lambda, GLR=x.GLR, DLR=x.DLR, kernel=x.kernel - 1), Neighbor.previous_kernel),
            ]
        else:
            neighbors = [
                Move(X(Lambda=x.Lambda + 1, GLR=x.GLR, DLR=x.DLR, kernel=x.kernel), Neighbor.next_lambda),
                Move(X(Lambda=x.Lambda - 1, GLR=x.GLR, DLR=x.DLR, kernel=x.kernel), Neighbor.previous_lambda),
                Move(X(Lambda=x.Lambda, GLR=x.GLR + 1, DLR=x.DLR, kernel=x.kernel), Neighbor.next_GLR),
                Move(X(Lambda=x.Lambda, GLR=x.GLR - 1, DLR=x.DLR, kernel=x.kernel), Neighbor.previous_GLR),
                Move(X(Lambda=x.Lambda, GLR=x.GLR, DLR=x.DLR + 1, kernel=x.kernel), Neighbor.next_DLR),
                Move(X(Lambda=x.Lambda, GLR=x.GLR, DLR=x.DLR - 1, kernel=x.kernel), Neighbor.previous_DLR),
                Move(X(Lambda=x.Lambda, GLR=x.GLR, DLR=x.DLR, kernel=x.kernel + 1), Neighbor.next_kernel),
            ]
        shuffle(neighbors)
        return neighbors

    def met_stop_condition(self):
        # Never stop
        return False

    def check_neighborhood(self):
        if self.k > self.k_max:
            self.k = 1

    def shaking(self):
        self.shaking_x = self.x
        for _ in range(self.k):
            move = VNS.find_neighbors(self.shaking_x)[0]
            self.shaking_x = move.x
        self.shaking_x_cost = self.shaking_x.evaluate()
        self.local_search_starts_with_x = False
        self.local_search_starts_with_shaking_x = True
        self.local_search_starts_with_local_search_x = False
        self.write_to_log(
            f"{'[Shaking]':>15} cost: {self.shaking_x_cost:<15.3} time: {self.get_elapsed_time():<15.3f} k: {self.k:2} parameters: Lambda=2^{self.shaking_x.Lambda}   GLR=2^{self.shaking_x.GLR}   DLR=2^{self.shaking_x.DLR}   kernel={self.shaking_x.kernel}\n"
        )

    def local_search(self):
        if self.local_search_starts_with_x:
            self.local_search_x = self.x
            self.local_search_x_cost = self.x_cost
        elif self.local_search_starts_with_shaking_x:
            self.local_search_x = self.shaking_x
            self.local_search_x_cost = self.shaking_x_cost
        else:
            assert self.local_search_starts_with_local_search_x
        found_local_minima = False
        while not found_local_minima:
            found_local_minima = True
            for move in VNS.find_neighbors(self.local_search_x):
                cost = move.x.evaluate()
                if cost < self.local_search_x_cost:
                    self.local_search_x = move.x
                    self.local_search_x_cost = cost
                    found_local_minima = False
                    self.write_to_log(
                        f"{'[LocalSearch]':>15} cost: {self.local_search_x_cost:<15.3} time: {self.get_elapsed_time():<15.3f} k: {self.k:2} parameters: Lambda=2^{self.local_search_x.Lambda}   GLR=2^{self.local_search_x.GLR}   DLR=2^{self.local_search_x.DLR}   kernel={self.local_search_x.kernel}   (reached by a '{move.direction.value} move')\n"
                    )
                    break

    def update(self):
        if self.local_search_x_cost < self.x_cost:
            self.x = self.local_search_x
            self.x_cost = self.local_search_x_cost
            self.k = 1
        else:
            self.k = self.k + 1
        self.write_to_log(
            f"{'[Update]':>15} cost: {self.x_cost:<15.3} time: {self.get_elapsed_time():<15.3f} k: {self.k:2} parameters: Lambda=2^{self.x.Lambda}   GLR=2^{self.x.GLR}   DLR=2^{self.x.DLR}   kernel={self.x.kernel}\n"
        )

    def run(self):
        if self.start_from_local_search:
            self.local_search()
            self.update()
        while not self.met_stop_condition():
            self.check_neighborhood()
            self.shaking()
            self.local_search()
            self.update()

    def plot_performance(log_file: str):
        def to_log10(x: float) -> str:
            expo = int(np.floor(np.log10(x)))
            base = x / 10 ** expo
            return f"{base:3.1f}" + "\\times 10^{" + f"{expo}" + "}"

        with open(log_file) as f:
            lines = f.readlines()
        _, last_time, _, _ = VNS.get_data_from_log_line(lines[-1])
        last_time = last_time / 3600  # seconds -> hours

        fig = plt.figure(figsize=(12, 5))
        for i, line in enumerate(lines):
            cost, time, k, x = VNS.get_data_from_log_line(line)
            time = time / 3600  # seconds -> hours
            if line.split()[0] == "[Init]":
                plt.plot([0, len(lines)-1], [cost, cost], "--k", linewidth=0.5)
                plt.text(len(lines)-1, cost, "  $" + to_log10(cost) + "$", horizontalalignment="left", verticalalignment="center")
            elif line.split()[0] == "[Shaking]":
                plt.plot([i-1, i], [last_step["cost"], cost], "--r")
            elif line.split()[0] == "[LocalSearch]":
                plt.plot([i-1, i], [last_step["cost"], cost], "-g")
            elif line.split()[0] == "[Update]":
                plt.plot([i-1, i], [last_step["cost"], last_step["cost"]], "-g")
                plt.plot([i, i], [last_step["cost"], cost], ":k")
                if k == 1:
                    plt.plot([0, len(lines)-1], [cost, cost], "--k", linewidth=0.5)
                    plt.text(
                        i-2,
                        cost,
                        " $\lambda="
                        + f"{int(2**x.Lambda)}"
                        + "$, $kernel="
                        + f"{x.kernel}"
                        + "$,\n$G_{LR}="
                        + to_log10(2 ** x.GLR)
                        + "$, $D_{LR}="
                        + to_log10(2 ** x.DLR)
                        + "$",
                        verticalalignment="bottom",
                        horizontalalignment="left",
                        rotation=60,
                    )
                    plt.text(len(lines)-1, cost, "  $" + to_log10(cost) + "$", horizontalalignment="left", verticalalignment="center")
            last_step = {"cost": cost, "time": time, "k": k, "x": x}

        plt.yscale("log")
        plt.xlim(0, len(lines)-1)
        plt.xlabel("VNS steps")
        plt.ylabel("$\sum||y-G(x,z)||_1$")
        plt.legend(
            handles=[
                Line2D([0], [0], color="g", marker=None, linestyle="-", label="Local Search"),
                Line2D([0], [0], color="r", marker=None, linestyle="--", label="Shaking"),
                Line2D([0], [0], color="k", marker=None, linestyle=":", label="Update"),
            ]
        )
        fig.savefig(log_file.replace(".txt", "") + ".pdf", format="pdf")

        plt.show()
        plt.close(fig=fig)
