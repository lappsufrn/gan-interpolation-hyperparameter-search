import os
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from utils import get_input_parameters

os.makedirs("graphs", exist_ok=True)

steps = 100000


parameters = [
    ("checkpoint_dir", ["./training_checkpoints"]),
    ("loss_norm", ["l1", "l2"]),
    ("train_ratio", ["0.7"]),
    ("gen_learning_rate", ["1e-4", "2e-4", "4e-4"]),
    ("disc_learning_rate", ["1e-4", "2e-4", "4e-4"]),
    ("batch_size", ["1", "2"]),
    ("lambda", ["10", "100", "1000"]),
    ("kernel", ["3", "4", "5", "6"]),
    ("steps", [str(steps)]),
    ("run_id", ["0"]),
]


def execute_configuration(param_id, configuration, execute_function):
    if param_id == len(parameters):
        return execute_function(configuration)
    else:
        dfs = []
        for value in parameters[param_id][1]:
            dfs += execute_configuration(param_id + 1, configuration + [(parameters[param_id][0], value)], execute_function)
        return dfs


def add_dataframe(configuration):
    params = [f"{c[0]}={c[1]}" for c in configuration]
    lamb = 0
    for c in configuration:
        if c[0]=="lambda":
            lamb = float(c[1])
            break
    _,checkpoint_full_dir,_,_,_ = get_input_parameters(params)
    if os.path.exists(checkpoint_full_dir + "/graphs.txt"):
        data = np.loadtxt(checkpoint_full_dir + "/graphs.txt")
        df_dict = {c[0]: c[1] for c in configuration}
        df_dict["step"] = data[:,0]
        df_dict["time"] = data[:,1]
        df_dict["gen_total_loss"] = data[:,2]
        df_dict["$G_{loss}$"] = data[:,3]
        df_dict["$\mathcal{L}_{L_n}$"] = data[:,4]
        df_dict["disc_loss"] = data[:,5]
        df_dict["$\mathcal{L}_{cGAN}$"] = -df_dict["disc_loss"]
        df_dict["$\mathcal{L}_{cGAN} + \lambda\mathcal{L}_{L_n}$"] = df_dict["$\mathcal{L}_{cGAN}$"] + lamb*df_dict["$\mathcal{L}_{L_n}$"]
        df_dict["$\sum||y-G(x,z)||_1$"] = data[:,6]
        df_dict["$\lambda\mathcal{L}_{L_n}/(|\mathcal{L}_{cGAN}| + \lambda\mathcal{L}_{L_n})$"] = lamb*df_dict["$\mathcal{L}_{L_n}$"]/(abs(df_dict["$\mathcal{L}_{cGAN}$"]) + lamb*df_dict["$\mathcal{L}_{L_n}$"])
        df = pd.DataFrame(df_dict)
        return [df]
    else:
        return []


if __name__ == '__main__':
    dfs = execute_configuration(0, [], add_dataframe)
    df = pd.concat(dfs).reset_index(drop=True)
    df = df[(df["step"]+1) % 500 == 0].reset_index(drop=True)

    print("Best solution:")
    last_steps_df = df[df["step"] == steps-1].reset_index(drop=True)
    print(last_steps_df.loc[last_steps_df["$\sum||y-G(x,z)||_1$"].argmin()])

    gen_lrs = df["gen_learning_rate"].unique()
    disc_lrs = df["disc_learning_rate"].unique()
    next_plot = 0

    for i,gen_lr in enumerate(gen_lrs):
        plt.figure(next_plot, figsize=(8,4))
        sns.lineplot(df[(df["gen_learning_rate"]==gen_lr)], x="step", y="$\sum||y-G(x,z)||_1$", hue="disc_learning_rate")
        plt.savefig(f"graphs/gen_learning_rate_{gen_lr}.pdf", format="pdf")
        plt.close(next_plot)
        next_plot += 1

    for i,disc_lr in enumerate(disc_lrs):
        plt.figure(next_plot, figsize=(8,4))
        sns.lineplot(df[(df["disc_learning_rate"]==disc_lr)], x="step", y="$\sum||y-G(x,z)||_1$", hue="gen_learning_rate")
        plt.savefig(f"graphs/disc_learning_rate_{disc_lr}.pdf", format="pdf")
        plt.close(next_plot)
        next_plot += 1

    next_plot = 3
    for i,param in enumerate([p[0] for p in parameters]):
        if param != "checkpoint_dir" and param != "train_ratio" and param != "steps" and param != "run_id":
            for metric in ["$\lambda\mathcal{L}_{L_n}/(|\mathcal{L}_{cGAN}| + \lambda\mathcal{L}_{L_n})$", "$\sum||y-G(x,z)||_1$"]:
                backslash = "\\"
                plt.figure(next_plot, figsize=(8,4))
                sns.lineplot(df, x="step", y=metric, hue=param)
                plt.savefig(f"graphs/all_{param}_{metric.replace('{','').replace('}','').replace(' ','').replace('|','').replace('$','').replace(f'lambda','lx').replace(f'{backslash}','').replace(f'mathbb','').replace(f'mathcal','').replace(f'/','over')}.pdf", format="pdf")
                plt.close(next_plot)
                next_plot += 1
