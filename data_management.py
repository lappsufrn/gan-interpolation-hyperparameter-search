from enum import Enum
from math import ceil
from typing import List, Tuple
import numpy as np
import tensorflow as tf
import cv2


class OverlapStrategy(Enum):
    replace = "replace"
    mean = "mean"


def load_bin(path: str, nshots: int, receivers: int, samples: int, dtype: str = "float32", verbose: bool = False):
    if verbose:
        print("Loadind Data .bin...")
    gather_n_bytes = receivers * samples * 4
    total_nbytes = gather_n_bytes

    shot_gathers = np.zeros((nshots, receivers, samples))

    with open(path, "rb") as f:
        for i in range(nshots):
            f.seek(i * total_nbytes)
            buf = f.read(total_nbytes)
            array = np.frombuffer(buf, dtype=dtype)
            shot_gathers[i] = array.reshape(receivers, samples)
    if verbose:
        print("Data loaded! ###################")
    return shot_gathers


def cut_shot(shot: np.ndarray, receivers: int, samples: int, verbose: bool = False):
    shot_rec = shot[:, :receivers, :samples]
    if verbose:
        print("Shape of cut shot ", shot_rec.shape)
    return shot_rec


def normalize(shot: np.ndarray, boundaries: Tuple[float, float] = (-1, 1), verbose=False):
    max_amp = np.max(shot)
    min_amp = np.min(shot)

    min_value = boundaries[0]
    max_value = boundaries[1]

    if verbose:
        print("Max amplitude = ", max_amp)
        print("Min amplitude = ", min_amp)

    shot_norm = (shot - min_amp) * ((max_value - min_value) / (max_amp - min_amp)) + min_value
    max_amp_norm = np.max(shot_norm)
    min_amp_norm = np.min(shot_norm)

    if verbose:
        print("Normalized max amplitude = ", max_amp_norm)
        print("Normalized min amplitude = ", min_amp_norm)
        print("Shape of shot gather", shot_norm.shape)
    return shot_norm, (min_amp, max_amp)


def decimate_gather(shot_gather: np.ndarray, verbose: bool = False):
    nshots = shot_gather.shape[0]
    receivers = shot_gather.shape[1]
    shot_decimated = np.array(shot_gather)
    for i in range(nshots):
        for j in range(1, receivers, 2):
            shot_decimated[i, j, :] = 0.0
    if verbose:
        print("Shape of decimated shot", shot_decimated.shape)
    return shot_gather, shot_decimated


def get_starting_points(matrix_size: Tuple[int, int], window_size: Tuple[int, int]) -> Tuple[List[int], List[int]]:
    vertical_windows = ceil(matrix_size[0] / window_size[0])
    vertical_starting_points = [i * (matrix_size[0] - window_size[0]) // (vertical_windows - 1) for i in range(vertical_windows)]
    horizontal_windows = ceil(matrix_size[1] / window_size[1])
    horizontal_starting_points = [i * (matrix_size[1] - window_size[1]) // (horizontal_windows - 1) for i in range(horizontal_windows)]
    return vertical_starting_points, horizontal_starting_points


def gen_windows(shot: np.ndarray, window_size: Tuple[int, int], verbose: str = False):
    if verbose:
        print("Generating windows from gathers")
    nshots = shot.shape[0]
    vertical_starting_points, horizontal_starting_points = get_starting_points((shot.shape[1], shot.shape[2]), window_size)
    vertical_windows = len(vertical_starting_points)
    horizontal_windows = len(horizontal_starting_points)

    windows = np.zeros((nshots * vertical_windows * horizontal_windows, window_size[0], window_size[1]))
    starting_points = np.zeros((nshots * vertical_windows * horizontal_windows, 3))

    k = 0
    for f in range(nshots):
        for i in range(vertical_windows):
            for j in range(horizontal_windows):
                windows[k, :, :] = shot[
                    f,
                    vertical_starting_points[i] : vertical_starting_points[i] + window_size[0],
                    horizontal_starting_points[j] : horizontal_starting_points[j] + window_size[1],
                ]
                starting_points[k, :] = [f, vertical_starting_points[i], horizontal_starting_points[j]]
                k += 1
    if verbose:
        print("Shape of windows ", windows.shape)
    return windows, starting_points, (vertical_windows, horizontal_windows)


def get_shots_from_windows(
    windows: np.ndarray,
    window_size: Tuple[int, int],
    starting_points: np.ndarray,
    num_windows: Tuple[int, int],
    overlap_strategy: OverlapStrategy = OverlapStrategy.replace,
):
    windows_per_shot = num_windows[0] * num_windows[1]
    nshots = int(windows.shape[0] / windows_per_shot)
    shots = np.zeros((nshots, int(max(starting_points[:, 1]) + window_size[0]), int(max(starting_points[:, 2]) + window_size[1]), 1))

    if overlap_strategy == OverlapStrategy.replace:
        for s in range(nshots):
            for w in range(s * windows_per_shot, (s + 1) * windows_per_shot):
                shots[
                    s,
                    int(starting_points[w, 1]) : int(starting_points[w, 1] + window_size[0]),
                    int(starting_points[w, 2]) : int(starting_points[w, 2] + window_size[1]),
                ] = windows[w, :, :]
    elif overlap_strategy == OverlapStrategy.mean:
        counts = np.zeros((shots.shape[0], shots.shape[1], shots.shape[2], 1), dtype=int)
        for s in range(nshots):
            for w in range(s * windows_per_shot, (s + 1) * windows_per_shot):
                shots[
                    s,
                    int(starting_points[w, 1]) : int(starting_points[w, 1] + window_size[0]),
                    int(starting_points[w, 2]) : int(starting_points[w, 2] + window_size[1]),
                ] += windows[w, :, :]
                counts[
                    s,
                    int(starting_points[w, 1]) : int(starting_points[w, 1] + window_size[0]),
                    int(starting_points[w, 2]) : int(starting_points[w, 2] + window_size[1]),
                ] += 1
        shots /= counts

    return shots


def resize(shot: np.ndarray, height: int, width: int):
    nshots = shot.shape[0]
    img_res = np.zeros((nshots, height, width, 1))
    aux = shot.reshape(nshots, shot.shape[1], shot.shape[2], 1)
    for i in range(nshots):
        #img_res[i] = tf.image.resize(aux[i], [height, width], method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
        img_res[i,:,:,0] = cv2.copyMakeBorder(aux[i,:,:,0], 3, 3, 3, 3, cv2.BORDER_CONSTANT, None, value = 0)
    img_res32 = np.float32(img_res)
    return img_res32

def resizeneg(shot: np.ndarray, height: int, width: int):
    nshots = shot.shape[0]
    img_res = np.zeros((nshots, height, width, 1))
    aux = shot.reshape(nshots, shot.shape[1], shot.shape[2], 1)
    for i in range(nshots):
        #img_res[i] = tf.image.resize(aux[i], [height, width], method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
        img_res[i,:,:,0] = aux[i,3:-3,3:-3,0]
    img_res32 = np.float32(img_res)
    return img_res32



def add_receivers(shot: np.ndarray):
    if len(shot.shape) == 3:
        augmented_shot = np.zeros((shot.shape[0], 2 * shot.shape[1], shot.shape[2]), dtype=shot.dtype)
    else:
        augmented_shot = np.zeros((shot.shape[0], 2 * shot.shape[1], shot.shape[2], 1), dtype=shot.dtype)
    augmented_shot[:, range(0, augmented_shot.shape[1], 2)] = shot
    return augmented_shot


def prepare_data(
    filename: str,
    nshots: int,
    receivers: int,
    samples: int,
    window_size: Tuple[int, int],
    cut_odd: bool = True,
    verbose: bool = False,
):
    shot_gather = load_bin(filename, nshots, receivers, samples, verbose=verbose)
    if cut_odd:
        shot_gather = cut_shot(
            shot_gather,
            receivers if receivers % 2 == 0 else receivers - 1,
            samples if samples % 2 == 0 else samples - 1,
            verbose=verbose,
        )
    shot_gather, old_boundaries = normalize(shot_gather, verbose=verbose)
    shot_gather, shot_decimated = decimate_gather(shot_gather, verbose=verbose)
    windowed_shot_gather, windows_starting_points, num_windows = gen_windows(shot_gather, window_size, verbose=verbose)
    windowed_shot_decimated, _, _ = gen_windows(shot_decimated, window_size, verbose=verbose)
    shot_gather_resized = resize(windowed_shot_gather, 256, 256)
    if verbose:
        print("Shot gather 256 shape", shot_gather_resized.shape)
    shot_decimated_resized = resize(windowed_shot_decimated, 256, 256)
    if verbose:
        print("Shot desimate 256 shape", shot_decimated_resized.shape)
    return (
        shot_gather,
        shot_decimated,
        shot_gather_resized,
        shot_decimated_resized,
        old_boundaries,
        windows_starting_points,
        num_windows,
    )
